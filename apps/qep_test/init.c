/*
 * Copyright (c) 2020 <james.fitzsimons@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <assert.h>

#include <rtems.h>
#include <bsp/beagleboneblack.h>
#include <bsp/pwmss.h>
#include <bsp/qep.h>
#include <bsp/gpio.h>


static void
Init(rtems_task_argument arg)
{
    printf("Starting QEP Testing\n");
    int32_t position = 0;
    rtems_status_code sc;

    /*Initialize GPIO pins in BBB*/
    rtems_gpio_initialize();

    // Select which PWMSS module we are going to be using.
    BBB_PWMSS pwmss_id = BBB_PWMSS1;

    /* Set P8 Header, pins 33 and 35 as inputs eQEP1 A_IN & B_IN */
    beagle_qep_pinmux_setup(BBB_P8_33_1B_IN, pwmss_id, true);
    beagle_qep_pinmux_setup(BBB_P8_35_1A_IN, pwmss_id, true);

    // Initialise the pwm module
    printf("Initialising QEP\n");
    sc = beagle_qep_init(pwmss_id);
    assert(sc == RTEMS_SUCCESSFUL);
    printf("Completed initialising QEP\n");

    while(1) {
        position = beagle_qep_get_position(pwmss_id);
        printf("Value of qep position is %d\n", position);
        sleep(1);
    }

}

#define CONFIGURE_MICROSECONDS_PER_TICK 1000

#define CONFIGURE_APPLICATION_NEEDS_CLOCK_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_CONSOLE_DRIVER

#define CONFIGURE_MAXIMUM_FILE_DESCRIPTORS 10
#define CONFIGURE_UNLIMITED_OBJECTS
#define CONFIGURE_UNIFIED_WORK_AREAS

#define CONFIGURE_USE_IMFS_AS_BASE_FILESYSTEM

#define CONFIGURE_RTEMS_INIT_TASKS_TABLE
#define CONFIGURE_INIT

#include <rtems/confdefs.h>
