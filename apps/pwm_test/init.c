/*
 * Copyright (c) 2020 <james.fitzsimons@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <assert.h>

#include <rtems.h>
#include <bsp/pwmss.h>
#include <bsp/bbb-pwm.h>


static void
Init(rtems_task_argument arg)
{
    printf("Starting PWM Testing\n");

    rtems_status_code sc;
    bool success;

    /*Initialize GPIO pins in BBB*/
    rtems_gpio_initialize();

    // Configure both channels of PWM instance 0 to run at 10 Hz, 50% duty cycle
    BBB_PWMSS pwmss_id = BBB_PWMSS1;
    float pwm_freq = 20000.0f; //Hz
    float duty_a = 50.0f;
    float duty_b = 50.0f;

    /* Set P9 Header , 21 Pin number , PWM B channel and 0 PWM instance to generate frequency*/
    beagle_pwm_pinmux_setup(BBB_P9_16_1B, pwmss_id);

    // Initialise the pwm module
    success = beagle_pwm_init(pwmss_id);
    assert(success == true);

    /* check clock is running */
    bool is_running = beagle_pwmss_is_running(pwmss_id);
    printf("clock is running %s\n", is_running ? "true" : "false");

    // Configure the PWM module for a 10Hz output at 50% duty cycle
    beagle_pwm_configure(pwmss_id, pwm_freq, duty_a, duty_b);

    printf("Enabling PWM  output....\n");
    // Enable the pwm output
    beagle_pwm_enable(pwmss_id);
    //while(1);

    for(duty_b = 50.0f; duty_b < 90.0f; duty_b += 5.0f) {
      printf("Duty at %f....\n", duty_b);
      beagle_pwm_configure(pwmss_id, pwm_freq, duty_a, duty_b);
      sleep(5);
    }

    /*freeze the counter and disable pwm module*/
    beagle_pwm_disable(pwmss_id);
    printf("PWM disabled. Test finished.\n");
}

#define CONFIGURE_MICROSECONDS_PER_TICK 1000

#define CONFIGURE_APPLICATION_NEEDS_CLOCK_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_CONSOLE_DRIVER

#define CONFIGURE_MAXIMUM_FILE_DESCRIPTORS 10
#define CONFIGURE_UNLIMITED_OBJECTS
#define CONFIGURE_UNIFIED_WORK_AREAS

#define CONFIGURE_USE_IMFS_AS_BASE_FILESYSTEM

#define CONFIGURE_RTEMS_INIT_TASKS_TABLE
#define CONFIGURE_INIT

#include <rtems/confdefs.h>
